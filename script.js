const mensagem = document.getElementById('Fim');
const enviar = document.getElementById('Enviar');
const excluir = document.getElementById('Excluir');
const editar = document.getElementById('Editar');
const codigo = document.getElementById('Msg');
const digitando = document.getElementById("Digitando");

enviar.addEventListener("click", function(){
  let novaM = mensagem.value;

  if (novaM !== ""){
    digitando.innerHTML += novaM + '\n';
    codigo.value += novaM + "\n"; 
    mensagem.value = ""; 
  }
});

excluir.addEventListener("click", function(){
  digitando.innerHTML = "";
  codigo.value = "";
});

editar.addEventListener("click", function(){
  const novaMensagem = prompt("Digite a nova mensagem:");

  if (novaMensagem !== null) {
    digitando.innerHTML = novaMensagem + '\n';
    codigo.value = novaMensagem + "\n";
  }
});
